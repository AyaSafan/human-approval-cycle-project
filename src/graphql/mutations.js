/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createHumanApprovalMessage = /* GraphQL */ `
  mutation CreateHumanApprovalMessage(
    $input: CreateHumanApprovalMessageInput!
    $condition: ModelHumanApprovalMessageConditionInput
  ) {
    createHumanApprovalMessage(input: $input, condition: $condition) {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
export const updateHumanApprovalMessage = /* GraphQL */ `
  mutation UpdateHumanApprovalMessage(
    $input: UpdateHumanApprovalMessageInput!
    $condition: ModelHumanApprovalMessageConditionInput
  ) {
    updateHumanApprovalMessage(input: $input, condition: $condition) {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
export const deleteHumanApprovalMessage = /* GraphQL */ `
  mutation DeleteHumanApprovalMessage(
    $input: DeleteHumanApprovalMessageInput!
    $condition: ModelHumanApprovalMessageConditionInput
  ) {
    deleteHumanApprovalMessage(input: $input, condition: $condition) {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
