/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateHumanApprovalMessage = /* GraphQL */ `
  subscription OnCreateHumanApprovalMessage {
    onCreateHumanApprovalMessage {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHumanApprovalMessage = /* GraphQL */ `
  subscription OnUpdateHumanApprovalMessage {
    onUpdateHumanApprovalMessage {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHumanApprovalMessage = /* GraphQL */ `
  subscription OnDeleteHumanApprovalMessage {
    onDeleteHumanApprovalMessage {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
