/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getHumanApprovalMessage = /* GraphQL */ `
  query GetHumanApprovalMessage($id: ID!) {
    getHumanApprovalMessage(id: $id) {
      id
      Name
      MessageAddress
      MessageSubject
      MessageBody
      createdAt
      updatedAt
    }
  }
`;
export const listHumanApprovalMessages = /* GraphQL */ `
  query ListHumanApprovalMessages(
    $filter: ModelHumanApprovalMessageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHumanApprovalMessages(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        Name
        MessageAddress
        MessageSubject
        MessageBody
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
