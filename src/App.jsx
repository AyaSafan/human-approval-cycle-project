
import React from 'react';
import './App.css';
import { useState } from 'react';
import { useEffect } from 'react';

import Form from './components/Form'
import Amplify, { API, graphqlOperation } from 'aws-amplify';
import {Auth} from 'aws-amplify';

import awsconfig from './aws-exports';
import { AmplifySignOut, withAuthenticator } from '@aws-amplify/ui-react';
import { listHumanApprovalMessages } from './graphql/queries';


Amplify.configure(awsconfig);

async function getUserInfo(setUserInfo) {
  const userInfo = await Auth.currentUserInfo();
  console.log('userInfo', userInfo);
  setUserInfo(userInfo);
}




function App() {
  
  const [messages, setMessages] = useState([]);
  const [userInfo, setUserInfo] = useState({"id":"","username":"","attributes":{}});
  useEffect(() => {
    getUserInfo(setUserInfo);
  }, []);

  


  useEffect(() => {
    fetchMessages();
  }, []);


  const fetchMessages = async () => {
    try {
        const messagesData = await API.graphql(graphqlOperation(listHumanApprovalMessages));
        const messagesList = messagesData.data.listHumanApprovalMessages.items;
        console.log('messages list ', messagesList);
        setMessages(messagesList);
    } catch (error) {
        console.log('error on fetching messages', error);
    }
  };

  
    return (
      <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark pt-2">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <h6  className="navbar-brand">{userInfo.attributes.email}</h6>
          </li>
        </ul>
        <AmplifySignOut /> 
      </nav>
      <br/>
      <div className='container'>
            <Form/>
            <br/><hr/>
            <div className="jumbotron">
            <h1>Your Approve/Reject Emails</h1>
            </div>
            <div className="messageList">
                {messages.map((message, idx) => {
                  if (message.MessageAddress === userInfo.attributes.email){
                    return (
                      <div key={`message${idx}`}>
                      <div className="card" >
                      <div className="card-header">
                       <h5> {message.MessageSubject}</h5>
                      </div>
                      <div className="card-body">
                        <p className="card-title"> to: {message.MessageAddress}</p>
                        <hr/>
                        <div dangerouslySetInnerHTML={{ __html: message.MessageBody }} />
                      </div>
                    </div>
                    <br/>
                    </div>
                    );
                  }
                  return null;
                })}
            </div>
      </div> 
      </div> 
        
    );
}

export default withAuthenticator(App, { includeGreetings: true })
